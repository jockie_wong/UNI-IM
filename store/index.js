import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		user: {
			home: {
				id: 1,
				name: 'tax',
				img: 'static/img/homeHL.png'
			},
			customer: {
				id: 2,
				name: 'customer',
				img: 'static/img/customerHL.png'
			},
			test:11111111
		}
	},
	mutations:{
		update(state,i){
			state.user.test = state.user.test + i;
		}
	},
	updated:function(){
		console.log('message update:'+ this.scrollTop);
	}
});

export default store
